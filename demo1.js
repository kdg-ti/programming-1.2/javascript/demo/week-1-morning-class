/*
let i = 7; // number (double)
i = 7.2; // number (double)
console.log('i = ' + i);
let s = 'Hello world';
console.log('s = ' + s);
s = true; // true or false
console.log("s = " + s);
let c = 'C'; // No char
const con = 'asdjhaskjd';
//con = '';  impossible

// Global scope
var v = 'sdfsdf';
v = 7;

if (true) { // BLOCK START
    // BLOCK
} // BLOCK END

{
    let l = 7;
    var v2 = 9;
    console.log('l = ' + l);
}
//console.log('l = ' + l);
console.log('v2 = ' + v2);
*/
/*
let v3 = undefined; // uncommon
let v4 = null;
if (v3 === undefined) {
    console.log('undef!');
}
if (v3 === null) {
    console.log('null!');
}
if (v3 === 0) {
    console.log('zero!');
}
if ('' === 0) {
    console.log('Yes, they are equal!');
} else {
    console.log('No');
}*/

//console.log("a" - 2); // JS will try ...  --> see the Book!
// NaN

//console.log(-1 * Infinity);

// Templating!
/*
const name = 'Lars';
const myString = `Hello, ${name}!`; // '' ""    A BIT like String.format
console.log(myString);
*/

// OPERATORS
//  / * % + - ++ -- post-and prefix notations ! == === > <=  PRECEDENCE : ( )

/*
const theType = typeof 'h';
console.log(theType);
console.log(theType.charAt(0));
*/

//let a = 7;
/*if (a === 89) {
    console.log();
} else if (false) {

} else {
    //
}*/
/*
while (a--) {
    console.log(a);
}
do {

} while (false);*/
/*
for (let i = 0; i < 10; i++) {
    console.log(i + 1);
    //break;
}
const n = 'Lars';
switch (n) {
    case 'Lars':
        console.log('Hello');
        break;
    case 'Jan':
        console.log('Hey');
        break;
    default:
        console.log(`${n} not supported!`);
}*/

//console.log('Root: ' + Math.sqrt(144)); // abs cos sinus round

// Browser:
//const userName = prompt('Please enter your name');
//alert('Hey!');
