console.log('------Task 2------')
let board = '';
let block1 = '# ';
let block2 = ' #';

for (let i = 0; i < 8; i++) {
    for (let j = 0; j < 4; j++) {
        if (i % 2 === 0)
            board += block1;
        else
            board += block2;
    }
    board += '\n'
}
console.log(board);
