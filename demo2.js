console.log(square(12));
console.log(box(12));
console.log(square('Hello'));
console.log(square());
doesSomething(7, 7, 8, 8);
const c = doesSomething(7);
console.log('c = ' + c);

/*const f3 = square;
console.log(f3(10));*/

function square(number) {
    return number * number;
}

function box(number) {
    return number * number * number;
}

function doesSomething(someNumber, someString) {
    console.log('someString: ' + someString);
}

// Non-void function
function f1() {
    if (true) {
        // No return
    } else {
        return '';
    }
}

const retValue = f1();
if (retValue === undefined) {
    //
}
